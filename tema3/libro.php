<?php 

require_once 'autor.php';

class TipoLibro {
  private $nombre;
  public function __construct(string $nombre)
  {
    $this->nombre = $nombre;
  }
}

class Libro
{
  public string $titulo, $editorial;
  public TipoLibro $tipo;
  public int $anio;
  public int $ISBN;
  public Array $autores; 

  public function __construct(string $titulo, TipoLibro $tipo, string $editorial, int $a, int $ISBN) {
    $this->titulo = $titulo;
    $this->tipo = $tipo; 
    $this->editorial = $editorial;
    $this->anio = $a;
    $this->ISBN = $ISBN;
    $this->autores = [];
  }
  
  public function agregarAutor(Autor $autor) {
    array_push($this->autores, $autor);
  }
}
