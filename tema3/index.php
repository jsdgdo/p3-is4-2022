<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Libro</title>
</head>
<?php 
  require_once 'libro.php'; 
  $t = new TipoLibro('Diseño');
  $l = new Libro('The Shape of Design', $t, 'Frank.Design', 2018, 132451345234623462);
  $frank = new Autor('Frank Chimero', 'US', strtotime("10 September 2000"));
  $elle = new Autor('Elle Lupton','US', strtotime("10 September 1998"));
  $l->agregarAutor($frank);
  $l->agregarAutor($elle);
?>
<body>
  <h1>Libro: <?php echo $l->titulo;?></h1>
  <h2>Autores</h2>
  <?php foreach ($l->autores as $autor) { 
    echo '<p>'.$autor->nombre.'</p>';
  } ?>
</body>
</html>