<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Datos del alumno</title>
</head>
<?php session_start(); ?>
<body>
  <h1>Datos el alumno</h1>
  <p>Nombre: <?php echo $_SESSION['nombre'];?></p>
  <p>Apellido: <?php echo $_SESSION['apellido'];?></p>
  <p>Matrícula: <?php echo $_SESSION['matricula'];?></p>
</body>
<?php session_destroy(); ?>
</html>