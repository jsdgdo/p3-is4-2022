<?php

namespace tema4;

class Alumno
{
  /**
   * Class constructor.
   */

  private $nombre, $apellido, $matricula;

  public function __construct()
  {
    $this->nombre = "José";
    $this->apellido = "Delgado";
    $this->matricula = "C06369";
  }

  public function getPage() {
    $_SESSION['nombre'] = $this->nombre;
    $_SESSION['apellido'] = $this->apellido;
    $_SESSION['matricula'] = $this->matricula;

    echo "Ir a <a href='/tema4/tema4_1.php'>tema4_1</a>";
  }
}

session_start();
$al = new Alumno();
$al->getPage();