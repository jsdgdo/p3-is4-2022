<?php 

class Autor
{
  public string $nombre, $nacionalidad;
  public int $f_nacimiento;

  /**
   * Class constructor.
   */
  public function __construct(string $name, string $nacionalidad, ?int $f_nacimiento)
  {
    $this->f_nacimiento = $f_nacimiento;
    $this->nombre = $name;
    $this->nacionalidad = $nacionalidad;
  }

}

